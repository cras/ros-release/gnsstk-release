## gnsstk (noetic) - 14.3.0-1

The packages in the `gnsstk` repository were released into the `noetic` distro by running `/usr/bin/bloom-release gnsstk -r noetic` on `Tue, 02 Apr 2024 12:50:37 -0000`

The `gnsstk` package was released.

Version of package(s) in repository `gnsstk`:

- upstream repository: https://github.com/SGL-UT/gnsstk.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/gnsstk-release.git
- rosdistro version: `14.0.0-8`
- old version: `14.0.0-8`
- new version: `14.3.0-1`

Versions of tools used:

- bloom version: `0.12.0`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## gnsstk (noetic) - 14.0.0-7

The packages in the `gnsstk` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --new-track -r noetic gnsstk` on `Fri, 20 Oct 2023 02:22:17 -0000`

The `gnsstk` package was released.

Version of package(s) in repository `gnsstk`:

- upstream repository: https://github.com/SGL-UT/gnsstk.git
- release repository: unknown
- rosdistro version: `null`
- old version: `14.0.0-6`
- new version: `14.0.0-7`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


